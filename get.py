#!/usr/bin/python3

from sys import argv
import csv
from datetime import date, timedelta
import requests
from bs4 import BeautifulSoup

if len(argv) != 3:
	exit(f'Usage: {argv[0]} <city> <year>')
city = argv[1]
year = int(argv[2])

with open(f'data/{city}-{year}.csv', 'w', newline='') as csvfile:
	csv_writer = csv.writer(csvfile)
	cur_date = date(year, 1, 1)
	end_date = date(year + 1, 1, 1)
	step_time = timedelta(days=1)
	session = requests.Session()
	while cur_date < end_date:
		month = cur_date.strftime('%m')
		day = cur_date.strftime('%d')
		url = f'https://ua.sinoptik.ua/погода-{city}/{year}-{month}-{day}'
		print(url)
		res = session.get(url)
		res.raise_for_status()
		soup = BeautifulSoup(res.content, 'lxml')
		table = soup.find('table', class_='weatherDetails').tbody
		td_num = 1
		while td := table.select(f'td:nth-of-type({td_num})'):
			time = int(td[0].string.split()[0])
			tmpr = int(td[2].string[:-1])
			hum = td[4].string
			wind = td[5].div
			wind_dir = next( s.split('-')[1] for s in wind['class'] if s.startswith('wind-') )
			csv_writer.writerow([ f'{year}-{month}-{day}T{time:02d}', tmpr, hum, wind.string, wind_dir ])
			td_num += 1
		cur_date += step_time

